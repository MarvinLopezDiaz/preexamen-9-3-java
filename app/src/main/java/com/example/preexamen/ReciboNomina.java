package com.example.preexamen;

public class ReciboNomina {
    private int numRecibo = 0;
    private String Nombre = "";
    private float horasTrabNormal = 0.0F;
    private float horasTrabExtra = 0.0F;
    private int puesto = 0;

    private float impuestoProc = 0.0F;

    public ReciboNomina(int numRecibo, String Nombre, float horasTrabNormal, float horasTrabExtra, int puesto, float impuestoProc) {
        this.numRecibo = numRecibo;
        this.Nombre = Nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtra = horasTrabExtra;
        this.puesto = puesto;
        this.impuestoProc = impuestoProc;
    }
    public int getNumRecivo() { return numRecibo; }

    public void setNumRecivo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() { return Nombre; }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public float getHorasTrabNormal() { return horasTrabNormal; }

    public void setHorasTrabNormal(float horasTrabNormal) { this.horasTrabNormal = horasTrabNormal; }

    public float getHorasTrabExtra() { return horasTrabExtra; }

    public void setHorasTrabExtra(float horasTrabExtra) { this.horasTrabExtra = horasTrabExtra; }

    public int getPuesto() { return puesto; }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoProc() { return impuestoProc; }

    public void setImpuestoProc(float impuestoProc) { this.impuestoProc = impuestoProc; }

    public float Calcular(float pago){
        float resultado;
        resultado = pago * horasTrabNormal;
        if(horasTrabExtra > 0){
            resultado = resultado + (horasTrabExtra * pago * 2);
        }
        return resultado;
    }

    public float CalcularSubtotal(){
        float subTo = 0.0F;
        switch (puesto){
            case 1: subTo = Calcular(240);
                    return subTo;
            case 2: subTo = Calcular(300);
                return subTo;
            case 3: subTo = Calcular(400);
                return subTo;
        }
        return subTo;
    }

    public float CalcularImpuesto(float subTo){
        impuestoProc = subTo * 0.16F;
        return impuestoProc;
    }

    public float CalcularTotal(float impuestoProc, float subTo){
        return subTo - impuestoProc;
    }
}
