package com.example.preexamen;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    private TextView lblNumRecibo;
    private TextView lblNombre;
    private TextView lblNombreMain;
    private EditText txtHorasNormal;
    private EditText txtHorasExtras;
    private RadioButton rdbAuxiliar;
    private RadioButton rdbAlbanil;
    private RadioButton rdbIngObra;
    private RadioGroup radioGrupo;
    private TextView lblImpuestoPor;
    private TextView lblSubtotal;
    private TextView lblimpuestp;
    private TextView lblTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private ReciboNomina recibo = new ReciboNomina(0, "", 0.0F,
            0.0F, 0, 0.0F);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        iniciarComponentes();
        Bundle bundle = getIntent().getExtras();
        lblNombre.setText(bundle.getString("nombre"));
        lblNombreMain.setText(bundle.getString("nombre"));


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Regresar();
            }
        });
    }

    public void iniciarComponentes(){
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        lblNombre =  findViewById(R.id.lblNombre);
        lblNombreMain = findViewById(R.id.lblNombreMain);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        rdbAuxiliar =  findViewById(R.id.rdbAuxiliar);
        rdbAlbanil = findViewById(R.id.rdbAlbanil);
        rdbIngObra = findViewById(R.id.rdbIngObra);
        radioGrupo = findViewById(R.id.grupoBotones);

        lblImpuestoPor = findViewById(R.id.lblImpuestoPor);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblimpuestp = findViewById(R.id.lblImpuestoPor);
        lblTotal = findViewById(R.id.lblTotal);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
    }

    private void calculoRep(){
        recibo.setHorasTrabNormal(Float.parseFloat(txtHorasNormal.getText().toString()));
        recibo.setHorasTrabExtra(Float.parseFloat(txtHorasExtras.getText().toString()));
        lblSubtotal.setText(String.valueOf(recibo.CalcularSubtotal()));
        lblImpuestoPor.setText(String.valueOf(recibo.CalcularImpuesto(Float.parseFloat(lblSubtotal.getText().toString()))));
        lblTotal.setText(String.valueOf(recibo.CalcularTotal(Float.parseFloat(lblImpuestoPor.getText().toString()),
                Float.parseFloat(lblSubtotal.getText().toString()))));
    }

    private void Calcular(){
        boolean validacion = rdbAuxiliar.isChecked() == false && rdbAlbanil.isChecked() == false && rdbIngObra.isChecked() == false;

        if(lblNumRecibo.getText().toString().trim().isEmpty() ||
                lblNombre.getText().toString().trim().isEmpty() ||
                txtHorasNormal.getText().toString().trim().isEmpty() ||
                txtHorasExtras.getText().toString().trim().isEmpty() || validacion){
            Toast.makeText(getApplicationContext(),
                    "Falta Ingresar Datos", Toast.LENGTH_LONG).show();
        }else{
            if (rdbAuxiliar.isChecked()){
                recibo.setPuesto(1);
                calculoRep();
            } else if (rdbAlbanil.isChecked()) {
                recibo.setPuesto(2);
                calculoRep();
            } else if (rdbIngObra.isChecked()) {
                recibo.setPuesto(3);
                calculoRep();
            }
        }
    }

    private void Limpiar(){
        lblNumRecibo.setText("");
        txtHorasNormal.setText("");
        txtHorasExtras.setText("");
        lblImpuestoPor.setText("");
        lblSubtotal.setText("");
        lblTotal.setText("");
    }

    private void Regresar(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Nomina");
        confirmar.setMessage(" ¿Desea regresar? ");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        confirmar.show();
    }

}